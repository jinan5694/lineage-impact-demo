# Lineage & impact
![lineage & impact](/public/banner2.png)
## 更新
最新版本v0.2.0
- 重新实现连接线，不使用第三方插件，解决全局绝对定位的问题
- 布局高低自适应
- 提供`render`方法，重新渲染布局（例如：左侧菜单宽度变化）
## 安装
```bash
npm i lineage-impact --save
```
### npm地址点[这里](https://www.npmjs.com/package/lineage-impact)
### 数据格式在[这里](https://gitee.com/jinan5694/lineage-impact-demo/blob/master/src/mock/source.js)
## 构建
```bash
npm run build-lib
# update version from  package.json
npm publish
```
## 使用
```html
<template>
  <div id="app">
    <button @click="$refs.lineage.render()">render</button>
    <button @click="handleChange">change</button>
    <Lineage ref="lineage" :source="source"></Lineage>
  </div>
</template>

<script>
import Lineage from './components/index.js'
import source from './mock/source.js'

export default {
  name: 'app',
  components: {
    Lineage
  },
  data () {
    return {
      source: null
    }
  },
  created () {
    this.queryData()
  },
  methods: {
    queryData () {
      setTimeout(() => {
        this.source = source
      }, 200);
    },
    handleChange () {
      this.source.downstream.push({
        type: {
          name: 'Lake',
          color: '#445566'
        },
        data: {
          name: 'Dynamic addition',
          icon: 'database',
          iconColor: 'blue'
        }
      })
    }
  }
}
</script>
```

