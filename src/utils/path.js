/**
 * 根据两点坐标返回曲线的d属性
 * @param {object} start 开始点
 * @param {object} end 结束点
 */

function path (start, end) {
  // start是否在 end 点的左侧
  const direction = start.x - end.x < 0 ? 'left' : 'right'

  // 为箭头位置修正曲线
  direction === 'left' ? end.x -= 8 : end.x += 8
  
  // 两点的坐标差
  const diffX = Math.abs(start.x - end.x)
  const diffY = Math.abs(start.y - end.y)

  // 中心点
  const c = {}
  c.x = Math.min(start.x, end.x) + diffX / 2
  c.y = Math.min(start.y, end.y) + diffY / 2

  // 曲率 
  const curv = diffY / diffX / 2

  // 控制点A
  const cpa = {}
  cpa.x = direction === 'left'
    ? start.x + diffX * curv
    : start.x - diffX * curv
  cpa.y = start.y

  // 控制点B
  const cpb = {}
  cpb.x = direction === 'left'
    ? end.x - diffX * curv
    : end.x + diffX * curv
  cpb.y = end.y

  // 贝塞尔曲线【起点，控制点a，控制点b，终点】
  return `M ${start.x} ${start.y} C ${cpa.x} ${cpa.y}, ${cpb.x} ${cpb.y}, ${end.x} ${end.y}`
}

export default path


