/**
 * 根据起始元素获取起始坐标
 */

 function coordinate (startElement, endElement) {
   if (!startElement || !endElement) {
    // console.error('startElement & endElement is undefined or not render completed.')
    return
   }

   const startCoordinate = {
     x: startElement.offsetLeft,
     y: startElement.offsetTop,
     width: startElement.offsetWidth,
     height: startElement.offsetHeight
   }
   const endCoordinate = {
     x: endElement.offsetLeft,
     y: endElement.offsetTop,
     width: endElement.offsetWidth,
     height: endElement.offsetHeight
   }
   const result = {start: {}, end: {}}

   if (startCoordinate.x < endCoordinate.x) {
    result.start.x = startCoordinate.x + startCoordinate.width
    result.start.y = startCoordinate.y + startCoordinate.height / 2

    result.end.x = endCoordinate.x
    result.end.y = endCoordinate.y + endCoordinate.height / 2
   } else {
    result.start.x = startCoordinate.x
    result.start.y = startCoordinate.y + startCoordinate.height / 2

    result.end.x = endCoordinate.x + endCoordinate.width
    result.end.y = endCoordinate.y + endCoordinate.height / 2
   }

   return result
 }

 export default coordinate