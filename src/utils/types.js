//  window 类型
const TYPES = {
  MODEL: {
    name: 'Model',
    color: '#4ba697'
  },
  LAKE: {
    name: 'Lake',
    color: '#954e4c'
  },
  REPORT: {
    name: 'Report',
    color: '#5a8afb'
  }
}

export {
  TYPES
}
