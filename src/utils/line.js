import shortid from 'shortid'

function setId (item, uuid, lineArray) {
  item.uuid = uuid
  if (item.upstream && item.upstream.length) {
    item.upstream.forEach(item => {
      const childId = shortid.generate()
      setId(item, childId, lineArray)
      lineArray.push({start: childId, end: uuid})
    })
  }
  if (item.downstream && item.downstream.length) {
    item.downstream.forEach(item => {
      const childId = shortid.generate()
      setId(item, childId, lineArray)
      lineArray.push({start: uuid, end: childId})
    })
  }
}

export default function (data) {
  const lineArray = []
  const uuid = shortid.generate()
  setId(data, uuid, lineArray)
  return {
    data: data,
    lineArray: lineArray
  }
}
