/**
 * 支持的类型，可以在这里新增
 */

export default function ({data}, types) {
  // 设置window title
  data._title = getTitle(data, types)

  switch (data._title) {
    case 'S3 File':
      return getS3Config(data)
    case 'Table':
      return getTableConfig(data)
    case 'Tableau':
      return getTableauConfig(data)
    default:
      return getDefaultConfig(data)
  }
}

function getTitle (data, types = []) {
  const item = types.find(item => item.value === data.type)
  return item && item.label || 'Unknown'
}

// items
function getS3Config (data) {
  return {
    itemType: 'itemFolder', // required
    // window
    title: data._title, // required
    windowColor: '#4ba697', // required
    // custom
    path: data.path,
    name: data.name
  }
}

function getTableConfig (data) {
  return {
    itemType: 'itemLabel', // required
    // window
    title: data._title, // required
    windowColor: '#954e4c', // required
    // custom
    label: data.name,
    icon: 'table',
    iconColor: '#fba65a'
  }
}

function getTableauConfig (data) {
  return {
    itemType: 'itemLabel', // required
    // window
    title: data._title, // required
    windowColor: '#5a8afb', // required
    // custom
    label: data.name,
    icon: 'Tableau',
    iconColor: '#ee3e3e'
  }
}

function getDefaultConfig (data) {
  return {
    itemType: 'itemLabel', // required
    // window
    title: data._title, // required
    windowColor: '#954e4c', // required
    // custom
    label: data.name,
    icon: 'table',
    iconColor: 'orange'
  }
}
