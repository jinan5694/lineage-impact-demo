// 字典数据 dam_resource_type，将字段传入即可

export default [
  {
    value: 'dam_resource_type_1',
    label: 'S3 File'
  },
  {
    value: 'dam_resource_type_2',
    label: 'Table'
  },
  {
    value: 'dam_resource_type_3',
    label: 'Tableau'
  }
]