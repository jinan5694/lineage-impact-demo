export default {
  // asset attrs
  data: {
    type: 'dam_resource_type_2',
    name: 'Sys_Area'
    // others
  },
  upstream: [
    {
      data: {
        type: 'dam_resource_type_1',
        name: 'area',
        path: 'Amazon S3/datalake-clean/EMR-Opp-Dims'
      }
    },
    {
      data: {
        type: 'dam_resource_type_1',
        name: 'dim_data',
        path: 'Amazon S3/datalake-clean/EMR-Opp-Dims'
      }
    }
  ],
  downstream: [
    {
      data: {
        type: 'dam_resource_type_3',
        name: 'dms summary daily report'
      }
    },
    {
      data: {
        type: 'dam_resource_type_3',
        name: 'Lead_Task Metedata'
      }
    }
  ]
}