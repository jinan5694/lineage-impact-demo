/**
 * 源数据结构设计
 */
import { TYPES } from '../utils/types'

export default {
  type: TYPES.MODEL,
  // data 结构不确定，只影响 window 窗口大小
  data: {
    id: 'aaa',
    name: 'Sys_Area',
    icon: 'table',
    iconColor: 'orange'
  },
  upstream: [
    // upstream
    {
      type: TYPES.LAKE,
      data: {
        id: 'bbb',
        name: 'Hdfs data area',
        icon: 'folder',
        iconColor: '#ee3e3e'
      },
      upstream: [
        {
          type: TYPES.LAKE,
          data: {
            id: 'ccc',
            name: 'Hdfs data area',
            icon: 'folder',
            iconColor: '#ee3e3e'
          }
        },
        {
          type: TYPES.LAKE,
          data: {
            id: 'ccc',
            name: 'Hdfs data area3',
            icon: 'folder',
            iconColor: '#ee3e3e'
          }
        }
      ]
    },
    {
      type: TYPES.LAKE,
      data: {
        id: 'bbb',
        name: 'Hdfs data area2',
        icon: 'folder',
        iconColor: '#ee3e3e'
      }
    },
    {
      type: TYPES.LAKE,
      data: {
        id: 'ddd',
        name: 'dim_data',
        icon: 'table',
        iconColor: 'orange'
      },
      upstream: [
        {
          type: TYPES.LAKE,
          data: {
            id: 'eee',
            name: 'Hdfs data area',
            icon: 'folder',
            iconColor: '#ee3e3e'
          }
        }
      ]
    },
  ],
  downstream: [
    {
      type: TYPES.REPORT,
      data: {
        id: 'fff',
        name: 'dms summary daily report',
        icon: 'database',
        iconColor: 'blue'
      }
    },
    {
      type: TYPES.MODEL,
      data: {
        id: 'ggg',
        name: 'Lead_Task Metedata',
        icon: 'table',
        iconColor: 'orange'
      }
    }
  ]
}
